const express = require('express');
const { Pool } = require("pg");

let port = Number(process.env.HTTP_PORT);
if (isNaN(port)) port = 8080;

const pool = new Pool();
const app = express();
app.use(express.json());

/*
 * Movie API
 */
app.get('/search/:query', (req, res) => {
    findMovie(req.params.query)
        .then(rows => res.json(rows.map(r => ({
            title: r.title,
            released: new Date(r.released).toDateString()
        }))))
        .catch(err => res.status(404).end("API server error" + err));
});

app.listen(port, () => console.log("Backend started on port", port));


async function findMovie(text) {
    const query = `SELECT * FROM movies WHERE LOWER(title) LIKE LOWER($1)`;
    const params = [ `%${text}%` ];
    const result = await pool.query(query, params);
    return result.rows;
}
